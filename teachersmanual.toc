\contentsline {chapter}{Contents}{1}% 
\contentsline {chapter}{\chapternumberline {1}History of Latin Methods.}{3}% 
\contentsline {section}{\numberline {1.1}Present deficiencies:}{3}% 
\contentsline {section}{\numberline {1.2}Former more successful methods:}{3}% 
\contentsline {section}{\numberline {1.3}The rejection of Medieval Latin:}{4}% 
\contentsline {section}{\numberline {1.4}A living Language changes:}{4}% 
\contentsline {section}{\numberline {1.5}When is a language a good language?}{4}% 
\contentsline {section}{\numberline {1.6}Is it a good means of communication?}{4}% 
\contentsline {section}{\numberline {1.7}Are works of artistic beauty written in it?:}{6}% 
\contentsline {section}{\numberline {1.8}The beginning of the grammar-analysis method:}{6}% 
\contentsline {section}{\numberline {1.9}Change in objectives of Latin teaching:}{7}% 
\contentsline {section}{\numberline {1.10}The Classical Investigation and mental discipline:}{7}% 
\contentsline {section}{\numberline {1.11}Latin for cultural values:}{8}% 
\contentsline {section}{\numberline {1.12}Conclusion:}{8}% 
\contentsline {chapter}{\chapternumberline {2}General principles of the natural method}{11}% 
\contentsline {section}{\numberline {2.1}Key principle: automatic habits:}{11}% 
\contentsline {section}{\numberline {2.2}The sense is paramount:}{12}% 
\contentsline {section}{\numberline {2.3}How the student should prepare a passage:}{12}% 
\contentsline {section}{\numberline {2.4}Teaching conversational Latin:}{13}% 
\contentsline {section}{\numberline {2.5}Latin Word Order:}{15}% 
\contentsline {chapter}{\chapternumberline {3}Specific suggestions for presenting each lesson}{27}% 
\contentsline {section}{\numberline {3.1}Preliminary comments:}{27}% 
\contentsline {section}{\numberline {3.2}Comments on Individual Lessons}{28}% 
\contentsline {chapter}{\chapternumberline {4}Set of examinations and sample oral questions}{47}% 
\contentsline {section}{\numberline {4.1}Exams for first year:}{47}% 
\contentsline {section}{\numberline {4.2}Exams for Second Year:}{50}% 
\contentsline {section}{\numberline {4.3}Sample Oral Questions:}{52}% 
\contentsline {chapter}{\chapternumberline {5}SUGGESTIONS FOR TEACHING SECOND YEAR: GENERAL NOTIONS}{55}% 
\contentsline {section}{\numberline {5.1}SPECIFIC SUGGESTIONS}{56}% 
\contentsline {chapter}{\chapternumberline {6}ADDENDUM : Sources of Collects, etc., of 2nd year}{59}% 
\contentsline {chapter}{\chapternumberline {7}KEY TO ENGLISH TO LATIN AND SCRAMBLES OF BOOK I}{61}% 
\contentsline {chapter}{\chapternumberline {8}KEY TO SCRAMBLES AND ENGLISH TO LATIN OF BOOK II}{77}% 
\contentsline {chapter}{\chapternumberline {9}How do you say it department}{85}% 
